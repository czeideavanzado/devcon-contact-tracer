DevCon Contact Tracer
---------------------

...

## Getting Started

The clone the repo:

```shell
git clone git@gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git
```

## Components

- [api-service](./api-service/README.md)
- [graph-service](./graph-service/README.md)
- [registration-service](./registration-service/README.md)

## Prerequisite

To run the services, the following tools are required:
- docker
- docker-compose

## Running to services:

Before running the services, make a copy of `.env.sample` to `.env` and change the variables as needed.

```shell
make run
```

Swagger doc will be accessible at [http://localhost:9999](http://localhost:9999)

## Stopping services

```shell
make stop
```

## Cleaning volumes

```shell
make clean
```



## TODO

Initial ToDo list:

- [ ] Improve this README
