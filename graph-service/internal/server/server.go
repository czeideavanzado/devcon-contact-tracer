package server

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/dctx/devcon-contact-tracer/graph-service/internal/handler"
)

type APIServer struct {
	*mux.Router
	*Config
	*handler.CheckinHandler
}

type Config struct {
	Host string
	Port int
}

func NewAPIServerConfig(host string, port int) *Config {
	return &Config{
		Host: host,
		Port: port,
	}
}

func NewAPIServer(apiserverConfig *Config, router *mux.Router, checkinHandler *handler.CheckinHandler) *APIServer {
	return &APIServer{
		Config:         apiserverConfig,
		Router:         router,
		CheckinHandler: checkinHandler,
	}
}

func (s *Config) String() string {
	return fmt.Sprintf("%s:%d", s.Host, s.Port)
}

// Run runs the server and it's services
func (a *APIServer) Run() error {
	address := a.Config.String()
	log.WithField("address", address).Info("API Server started")
	return http.ListenAndServe(address, a.Router)
}
