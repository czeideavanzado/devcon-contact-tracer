DOCKER_REGISTRY=docker.io/dctx
APP=graph-service
VERSION=latest
IMAGE=${DOCKER_REGISTRY}/${APP}:${VERSION}

all: clean lint build test package

clean:
	@echo '========================================'
	@echo ' Cleaning project'
	@echo '========================================'
	go clean
	rm -rf build | true
	@echo 'Done.'

deps:
	@echo '========================================'
	@echo ' Getting Dependencies'
	@echo '========================================'
	@echo 'Cleaning up dependency list...'
	go mod tidy
	@echo 'Downloading dependencies...'
	go mod download
	# @echo 'Vendorizing project dependencies...'
	# go mod vendor
	# @echo 'Done.'

build: deps
	@echo '========================================'
	@echo ' Building project'
	@echo '========================================'
	go fmt ./...
	@go build -o build/bin/${APP} -ldflags "-X main.version=${VERSION} -w -s" .
	@echo 'Done.'

test:
	@echo '========================================'
	@echo ' Running tests'
	@echo '========================================'
	@go test ./...
	@echo 'Done.'

lint:
	@echo '========================================'
	@echo ' Running lint'
	@echo '========================================'
	@golint ./...
	@echo 'Done.'

migrate: build
	@echo '========================================'
	@echo ' Running migrations'
	@echo '========================================'
	@build/bin/${APP} migrate
	@echo 'Done.'

run: build
	@echo '========================================'
	@echo ' Running application'
	@echo '========================================'
	@build/bin/${APP} serve
	@echo 'Done.'

package-image:
	@echo '========================================'
	@echo ' Packaging docker image'
	@echo '========================================'
	docker build -t ${IMAGE} .
	@echo 'Done.'

package-chart:
	@echo '========================================'
	@echo ' Packaging chart'
	@echo '========================================'
	mkdir -p build/chart
	cp -r helm/${APP} build/chart
	helm package  --app-version ${APP} -u -d build/chart build/chart/${APP}
	@echo 'Done.'

package: package-image
	
publish-image: package-image
	@echo '========================================'
	@echo ' Publishing image'
	@echo '========================================'
	docker push ${IMAGE}
	@echo 'Done.'

# publish-chart: package-chart
# 	@echo '========================================'
# 	@echo ' Publishing chart'
# 	@echo '========================================'
# 	helm push build/chart/${APP} ${PROJECT}
# 	@echo 'Done.'

publish: publish-image # publish-chart
