module gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service

go 1.14

require (
	github.com/google/uuid v1.1.1
	github.com/google/wire v0.4.0
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.6
	github.com/spf13/viper v1.6.2
	gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/pkg/transport v0.0.0-20200321082142-573f4d8e00bb
)
