package utils

import (
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"

	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/model/errors"
)

func ReadEntity(req *http.Request, entity interface{}) errors.JSONErrors {
	body := req.Body
	defer body.Close()
	serializedJSON, err := ioutil.ReadAll(body)
	if err != nil {
		return errors.New().Add(
			"400",
			map[string]string{"pointer": "/data"},
			"Unable to read request",
			"Request body is not readable")
	}
	return FromJSON(serializedJSON, entity)
}

func WriteEntity(res http.ResponseWriter, code int, entity interface{}) {
	serializedJSON, err := ToJSON(entity)
	if err != nil {
		WriteError(res, http.StatusInternalServerError, err)
		return
	}

	if _, wErr := res.Write(serializedJSON); wErr != nil {
		WriteError(res, http.StatusInternalServerError, errors.New().Add(
			"500",
			map[string]string{"pointer": "/data"},
			"Unable to write response",
			"Response body is not writable",
		))
		return
	}
	res.WriteHeader(code)
}

func WriteError(res http.ResponseWriter, code int, jsonErr errors.JSONErrors) {
	res.WriteHeader(code)
	serializedJSON, err := ToJSON(jsonErr)
	if err != nil {
		log.Error("unable to serialize error response")
		return
	}
	if _, wErr := res.Write(serializedJSON); wErr != nil {
		log.WithError(wErr).Error("unable to write error response")
	}
}
