package errors

import "fmt"

type JSONErrors []JSONError

type JSONError struct {
	Status string            `json:"status"`
	Source map[string]string `json:"source"`
	Title  string            `json:"title"`
	Detail string            `json:"detail"`
}

func New() JSONErrors {
	return JSONErrors{}
}

func (err JSONErrors) Add(status string, source map[string]string, title, detail string) JSONErrors {
	e := JSONError{
		Status: status,
		Source: source,
		Title:  title,
		Detail: detail,
	}
	return append(err, e)
}

func (je JSONError) Error() string {
	return fmt.Sprintf("%s:%s:%s", je.Status, je.Title, je.Detail)
}
