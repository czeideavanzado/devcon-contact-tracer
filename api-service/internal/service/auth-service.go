package service

import (
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/model"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/pkg/transport"
)

type AuthService struct {
	transport.MessageBus
}

func NewAuthService(mb transport.MessageBus) *AuthService {
	return &AuthService{mb}
}

func (s *AuthService) Register(register *model.Register) error {
	return nil
}
