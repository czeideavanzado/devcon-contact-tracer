package handler

import (
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/service"
)

type IncidentHandler struct {
	*service.IncidentService
}

func NewIncidentHandler(s *service.IncidentService) *IncidentHandler {
	handler := &IncidentHandler{IncidentService: s}
	return handler
}

func (h *IncidentHandler) Register(router *mux.Router) {
	incidentRoute := router.PathPrefix("/incident")
	incidentRoute.
		Methods(http.MethodPost, http.MethodOptions).
		Path("/report").
		Handler(reportIncident(h.IncidentService))
	log.Info("[POST] incident/report registered")
}

func reportIncident(incidentService *service.IncidentService) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		// get request model

		// if error, return 500??
		if err := incidentService.Report(nil); err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
		}

		res.WriteHeader(http.StatusOK)
	}

}
