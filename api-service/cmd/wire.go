//+build wireinject

package cmd

import (
	"github.com/google/wire"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/handler"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/server"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/api-service/internal/service"
	"gitlab.com/dctx/contact-tracer/devcon-contact-tracer.git/pkg/transport"
)

func createServer() *server.APIServer {
	wire.Build(

		ProvideKafka,
		service.NewAuthService,
		service.NewLocationService,
		service.NewIncidentService,

		mux.NewRouter,

		handler.NewAuthHandler,
		handler.NewLocationHandler,
		handler.NewIncidentHandler,

		ProvideHandlers,
		// ProvideFilters,

		ProvideServerConfig,
		server.NewAPIServer,
	)
	return &server.APIServer{}
}

func ProvideServerConfig() *server.Config {
	return server.NewAPIServerConfig(
		viper.GetString("server.host"),
		viper.GetInt("server.port"),
		viper.GetString("server.spec"),
		viper.GetStringSlice("server.cors.allowedOrigins"),
		viper.GetStringSlice("server.cors.allowedHeaders"),
		viper.GetStringSlice("server.cors.allowedMethods"),
	)
}

func ProvideKafka() transport.MessageBus {
	kafkaConfig := transport.NewKafkaConfig(
		viper.GetStringSlice("kafka.brokers"),
		viper.GetString("kafka.topic"),
		viper.GetInt("kafka.partition"),
		viper.GetInt("kafka.minBytes"),
		viper.GetInt("kafka.maxBytes"))
	return transport.NewKafka(kafkaConfig)
}

func ProvideHandlers(a *handler.AuthHandler, i *handler.IncidentHandler, l *handler.LocationHandler) []handler.Routable {
	return []handler.Routable{a, i, l}
}
