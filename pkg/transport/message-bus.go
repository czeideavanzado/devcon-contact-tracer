package transport

import (
	"io"
)

type Message struct {
	Key   []byte
	Value []byte
}

type OnMessageReceived func(*Message) error

type MessageBus interface {
	Publish(...Message) error
	Subscribe(offset int64, callback OnMessageReceived) (io.Closer, error)
}
