package transport

import (
	"context"
	"io"

	"github.com/segmentio/kafka-go"
)

const (
	DefaultMinBytes = 10e3
	DefaultMaxBytes = 10e6
)

type Kafka struct {
	kafka.WriterConfig
	kafka.ReaderConfig
}

type KafkaConfig struct {
	Brokers   []string
	Topic     string
	Partition int
	MinBytes  int
	MaxBytes  int
}

func NewKafkaConfig(brokers []string, topic string, partition, minBytes, maxBytes int) *KafkaConfig {
	return &KafkaConfig{
		Brokers:   brokers,
		Topic:     topic,
		Partition: partition,
		MinBytes:  minBytes,
		MaxBytes:  maxBytes,
	}
}

func NewKafka(config *KafkaConfig) MessageBus {
	if config.MinBytes == 0 {
		config.MinBytes = DefaultMinBytes
	}
	if config.MaxBytes == 0 {
		config.MaxBytes = DefaultMaxBytes
	}
	wconf := kafka.WriterConfig{
		Brokers:  config.Brokers,
		Topic:    config.Topic,
		Balancer: &kafka.LeastBytes{},
	}
	rconf := kafka.ReaderConfig{
		Brokers:   config.Brokers,
		Topic:     config.Topic,
		Partition: config.Partition,
		MinBytes:  config.MinBytes,
		MaxBytes:  config.MaxBytes,
	}
	return &Kafka{
		WriterConfig: wconf,
		ReaderConfig: rconf,
	}
}

func (k *Kafka) Publish(msgs ...Message) error {
	writer := kafka.NewWriter(k.WriterConfig)
	defer writer.Close()
	messages := make([]kafka.Message, len(msgs))
	for i, msg := range msgs {
		messages[i] = toKafkaMessage(msg)
	}
	return writer.WriteMessages(context.Background(), messages...)
}

func (k *Kafka) Subscribe(offset int64, callback OnMessageReceived) (io.Closer, error) {
	reader := kafka.NewReader(k.ReaderConfig)
	if offset != 0 {
		if err := reader.SetOffset(offset); err != nil {
			return nil, err
		}
	}
	go func() {
		for {
			message, err := reader.ReadMessage(context.Background())
			if err != nil {
				// should log
				// stop routine if there's a read error
				break
			}
			msg := fromKafkaMessage(message)
			if err := callback(msg); err != nil {
				// log that there's an error with the callback
				// close reader and break
				if err := reader.Close(); err != nil {
					// nothing to do? just log
				}
				break
			}
		}
	}()
	return reader, nil
}

func toKafkaMessage(msg Message) kafka.Message {
	return kafka.Message{
		Key:   msg.Key,
		Value: msg.Value,
	}
}

func fromKafkaMessage(message kafka.Message) *Message {
	return &Message{
		Key:   message.Key,
		Value: message.Value,
	}
}
